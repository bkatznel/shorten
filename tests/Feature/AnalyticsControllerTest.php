<?php

namespace Tests\Feature;

use App\Analytics;
use Illuminate\Http\Response;
use Laravel\Passport\Passport;
use Tests\TestCase;
use App\User;
use App\Link;

class AnalyticsControllerTest extends TestCase
{
    /** @test */
    function show_analytics_successfully()
    {
        /** @var User $user */
        $user = factory(User::class)->create();
        Passport::actingAs($user, ['link-analytics']);

        /** @var Link $link */
        $link = factory(Link::class)->create(['user_id' => $user->id]);

        factory(Analytics::class, 15)->create(['link_slug' => $link->slug]);

        $this->getJson('/api/link/analytics/'.$link->slug)
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonCount(15, 'data');
    }

    /** @test */
    function show_analytics_returns_401_for_invalid_authentication()
    {
        $this->getJson('/api/link/analytics/xxx')
            ->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    /** @test */
    function show_analytics_returns_403_when_attempting_to_view_a_link_they_did_not_create()
    {
        /** @var User $user */
        $user = factory(User::class)->create();
        $otherUser = factory(User::class)->create();
        Passport::actingAs($user, ['link-analytics']);

        /** @var Link $link */
        $link = factory(Link::class)->create(['user_id' => $otherUser->id]);

        factory(Analytics::class, 15)->create(['link_slug' => $link->slug]);

        $this->getJson('/api/link/analytics/'.$link->slug)
            ->assertStatus(Response::HTTP_FORBIDDEN);
    }
}
