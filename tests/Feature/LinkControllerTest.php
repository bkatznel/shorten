<?php

namespace Tests\Feature;

use Illuminate\Http\Response;
use Laravel\Passport\Passport;
use Tests\TestCase;
use App\User;
use App\Link;
use Faker\Generator as Faker;

class LinkControllerTest extends TestCase
{
    /** @test */
    function index_display_multiple_links_successfully()
    {
        /** @var User $user */
        $user = factory(User::class)->create();
        Passport::actingAs($user, ['link-list']);

        factory(Link::class, 15)->create(['user_id' => $user->id]);

        $this->getJson('/api/link')
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonCount(15, 'data');
    }

    /** @test */
    function index_return_401_for_invalid_authentication()
    {
        $this->getJson('/api/link')
            ->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    /** @test */
    function show_link_successfully()
    {
        /** @var User $user */
        $user = factory(User::class)->create();
        Passport::actingAs($user, ['link-show']);

        /** @var Link $link */
        $link = factory(Link::class)->create(['user_id' => $user->id]);

        $this->getJson('/api/link/'.$link->slug)
            ->assertStatus(Response::HTTP_OK);
    }

    /** @test */
    function show_link_returns_401_for_invalid_authentication()
    {
        /** @var Link $link */
        $link = factory(Link::class)->create(['user_id' => factory(User::class)->create()->id]);

        $this->getJson('/api/link/'.$link->slug)
            ->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    /** @test */
    function show_link_returns_403_when_attempting_to_view_a_link_they_did_not_create()
    {
        /** @var User $user */
        $user = factory(User::class)->create();
        $user2 = factory(User::class)->create();
        Passport::actingAs($user, ['link-show']);

        /** @var Link $link */
        $link = factory(Link::class)->create(['user_id' => $user2->id]);

        $this->getJson('/api/link/'.$link->slug)
            ->assertStatus(Response::HTTP_FORBIDDEN);
    }

    /** @test */
    function store_link_successfully()
    {
        Passport::actingAs(factory(User::class)->create(), ['link-create']);

        $this->postJson('/api/link/', [
            'destination' => $destination = app()->make(Faker::class)->url,
        ])->assertStatus(Response::HTTP_CREATED);

        $this->assertDatabaseHas('links', [
            'destination' => $destination,
        ]);
    }

    /** @test */
    function store_link_returns_401_for_invalid_authentication()
    {
        $this->postJson('/api/link/', [
            'destination' => $destination = app()->make(Faker::class)->url,
        ])->assertStatus(Response::HTTP_UNAUTHORIZED);

        $this->assertDatabaseMissing('links', [
            'destination' => $destination,
        ]);
    }
}
