<?php

namespace Tests\Feature;

use App\Jobs\LogAnalytics;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;
use App\User;
use App\Link;

class DestinationControllerTest extends TestCase
{
    /** @test */
    public function destination_controller_successfully_redirects()
    {
        Queue::fake();

        /** @var Link $link */
        $link = factory(Link::class)->create(['user_id' => factory(User::class)->create()->id]);

        $this->get('/'.$link->slug)->assertStatus(Response::HTTP_FOUND);

        Queue::assertPushed(LogAnalytics::class);
    }
}
