<?php

namespace Tests\Unit;

use App\Jobs\LogAnalytics;
use App\Link;
use App\User;
use Tests\TestCase;
use Faker\Generator as Faker;

class LogAnalyticsTest extends TestCase
{
    /** @test */
    function log_analytics_creates_analytics_record()
    {
        /** @var Faker $faker */
        $faker = app()->make(Faker::class);

        /** @var Link $link */
        $link = factory(Link::class)->create(['user_id' => factory(User::class)->create()->id]);

        (new LogAnalytics($faker->ipv4, $faker->userAgent, $link->slug))->handle();

        $this->assertDatabaseHas('analytics', [
            'link_slug' => $link->slug,
        ]);
    }

    /** @test */
    function log_analytics_increments_visits_counter()
    {
        /** @var Faker $faker */
        $faker = app()->make(Faker::class);

        /** @var Link $link */
        $link = factory(Link::class)->create(['user_id' => factory(User::class)->create()->id]);

        (new LogAnalytics($faker->ipv4, $faker->userAgent, $link->slug))->handle();

        $this->assertDatabaseHas('links', [
            'slug' => $link->slug,
            'visits' => 1,
        ]);
    }
}
