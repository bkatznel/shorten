<?php

namespace Tests\Unit;

use App\Services\Slug;
use Tests\TestCase;

class SlugTest extends TestCase
{
    /** @test */
    function slug_string_is_the_correct_length()
    {
        $slug = new Slug;
        $this->assertEquals(Slug::NUM_CHARS_IN_SLUG, strlen($slug()));
    }
}
