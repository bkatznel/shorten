<?php

use App\Services\Slug;
use Faker\Generator as Faker;

$factory->define(App\Link::class, function (Faker $faker) {
    return [
        'slug' => new Slug,
        'destination' => $faker->url,
        'visits' => 0,
    ];
});
