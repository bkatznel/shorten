<?php

use Faker\Generator as Faker;

$factory->define(App\Analytics::class, function (Faker $faker) {
    return [
        'ip' => $faker->ipv4,
        'user_agent' => $faker->userAgent,
        'country' => $faker->country,
        'city' => $faker->city,
        'region' => $faker->word,
        'isp' => $faker->sentence,
    ];
});
