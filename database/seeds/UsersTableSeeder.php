<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        // Create 10 users
        factory(App\User::class)->create(['name' => 'Beni Katznelson', 'email' => 'beni@katznelson.ca']);
        factory(App\User::class, 9)->create();
    }
}
