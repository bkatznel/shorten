<?php

use App\User;
use Illuminate\Database\Seeder;

class LinksTableSeeder extends Seeder
{
    public function run()
    {
        User::all()->each(function(User $user){
            factory(App\Link::class, 10)->create(['user_id' => $user->id]);
        });
    }
}
