<?php

namespace App\Services;

class Slug
{
    const NUM_CHARS_IN_SLUG = 7;
    const ALPHABET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    // After implementing this function I noticed that this can also be represented as a base62. Using a base62
    // implementation I could have continued using an autoincrement as my primary key in mysql and just converted
    // between base62 and base10 when looking up the slug in mysql. That strategy would also have the benefit of
    // never requiring a re-roll in the case of collisions. Since I have already committed to this strategy I will leave
    // this comment here as a discussion point.

    public function __invoke() : string
    {
        $slug = '';
        for($i=0; $i < self::NUM_CHARS_IN_SLUG; $i++)
        {
            $slug .= self::ALPHABET[rand(0,strlen(self::ALPHABET)-1)];
        }

        return $slug;
    }
}
