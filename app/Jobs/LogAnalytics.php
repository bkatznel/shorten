<?php

namespace App\Jobs;

use App\Analytics;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Sujip\Ipstack\Ipstack;

class LogAnalytics implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $ip;
    private $userAgent;
    private $slug;

    public function __construct($ip, $userAgent, $slug)
    {
        $this->ip = $ip;
        $this->userAgent = $userAgent;
        $this->slug = $slug;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $ipstack = new Ipstack($this->ip);

        Analytics::query()->create([
            'link_slug' => $this->slug,
            'ip' => $this->ip,
            'user_agent' => $this->userAgent,
            'country' => $ipstack->country(),
            'city' => $ipstack->city(),
            'region' => $ipstack->region(),
            'isp' => $ipstack->isp()
        ]);

        DB::table('links')->where('slug', '=', $this->slug)->increment('visits');

    }
}
