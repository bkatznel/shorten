<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $id
 * @property string $link_slug
 * @property string $user_agent
 * @property string $ip
 * @property string|null $country
 * @property string|null $city
 * @property string|null $region
 * @property string|null $isp
 * @property Link $link
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class Analytics extends Model
{
    protected $fillable = [
        'link_slug',
        'ip',
        'user_agent',
        'country',
        'city',
        'region',
        'isp',
    ];

    public function link()
    {
        return $this->belongsTo(Link::class, 'link_slug');
    }
}
