<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $slug
 * @property string $destination
 * @property User $user
 * @property int $visits
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class Link extends Model
{
    protected $primaryKey = 'slug';
    protected $keyType = 'string';
    public $incrementing = false;

    protected $fillable = [
        'slug',
        'destination',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function redirects()
    {
        return $this->hasMany(Analytics::class);
    }
}
