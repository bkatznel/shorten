<?php

namespace App\Http\Transformers;

use App\Link;
use League\Fractal\TransformerAbstract;

class LinkTransformer extends TransformerAbstract
{
    public function transform(Link $link)
    {
        return [
            'id' => $link->slug,
            'url' => config('app.url').'/'.$link->slug,
            'destination' => $link->destination,
            'visits' => (int)$link->visits,
            'created_at' => $link->created_at->toIso8601String(),
            'updated_at' => $link->created_at->toIso8601String(),
        ];
    }
}
