<?php

namespace App\Http\Transformers;

use App\Analytics;
use League\Fractal\TransformerAbstract;

class AnalyticsTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'link'
    ];

    public function transform(Analytics $analytics)
    {
        return [
            'id' => $analytics->id,
            'ip' => $analytics->ip,
            'user_agent' => $analytics->user_agent,
            'country' => $analytics->country,
            'city' => $analytics->city,
            'region' => $analytics->region,
            'isp' => $analytics->isp,
            'created_at' => $analytics->created_at->toIso8601String(),
            'updated_at' => $analytics->created_at->toIso8601String(),
        ];
    }

    public function includeLink(Analytics $analytics)
    {
        return $this->item($analytics->link, new LinkTransformer(), 'link');
    }
}
