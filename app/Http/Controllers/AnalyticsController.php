<?php

namespace App\Http\Controllers;

use App\Analytics;
use App\Http\Requests\ShowRequest;
use App\Http\Transformers\AnalyticsTransformer;
use App\Http\Transformers\LinkTransformer;
use Illuminate\Pagination\Paginator;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Spatie\Fractal\Fractal;

class AnalyticsController extends Controller
{
    public function __invoke(ShowRequest $request)
    {
        /** @var Paginator|\Illuminate\Contracts\Pagination\LengthAwarePaginator $paginator */
        return response()->json(
            Fractal::create()
                ->paginateWith(new IlluminatePaginatorAdapter(
                    $paginator = Analytics::query()->where('link_slug', $request->route('link'))->paginate()
                ))
                ->collection(
                    $paginator->getCollection(),
                    AnalyticsTransformer::class,
                    'analytics'
                )
                ->parseIncludes('link')
        );
    }
}
