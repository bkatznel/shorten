<?php

namespace App\Http\Controllers;

use App\Http\Requests\ShowRequest;
use App\Http\Requests\StoreRequest;
use App\Http\Transformers\LinkTransformer;
use App\Link;
use App\Services\Slug;
use Illuminate\Http\Response;
use Illuminate\Pagination\Paginator;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Spatie\Fractal\Fractal;

class LinkController extends Controller
{
    public function index()
    {
        /** @var Paginator|\Illuminate\Contracts\Pagination\LengthAwarePaginator $paginator */
        return response()->json(
            Fractal::create()
                ->paginateWith(new IlluminatePaginatorAdapter(
                    $paginator = Link::query()->where('user_id', auth()->id())->paginate()
                ))
                ->collection(
                    $paginator->getCollection(),
                    LinkTransformer::class,
                    'link'
                )
        );
    }

    public function store(StoreRequest $request)
    {
        return response()->json(
            Fractal::create()->item(
                $this->createLink($request),
                LinkTransformer::class,
                'link'
            ),
            Response::HTTP_CREATED
        );
    }

    public function show(ShowRequest $request)
    {
        $link = Link::query()
            ->where('slug', $request->route('link'))
            ->where('user_id', '=', auth()->id())
            ->first();

        return response()->json(
            Fractal::create()
                ->item($link, LinkTransformer::class, 'link')
        );
    }

    private function createLink(StoreRequest $request)
    {
        try {
            $link = Link::query()->create([
                'slug'        => (new Slug)(),
                'user_id'     => auth()->id(),
                'destination' => $request->input('destination'),
            ]);
        } catch (\Illuminate\Database\QueryException $e) {
            // If we run into a slug that is already in use
            // re-roll for another slug
            return $this->createLink($request);
        }
        return $link;
    }
}
