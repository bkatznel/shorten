<?php

namespace App\Http\Controllers;

use App\Jobs\LogAnalytics;
use App\Link;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redirect;

class DestinationController extends Controller
{
    use DispatchesJobs;

    const CACHE_PREFIX = 'link';
    const CACHE_IN_MINUTES = 43200; //minutes

    public function __invoke(Request $request)
    {
        $link = Cache::remember('link'.$request->route('link'), self::CACHE_IN_MINUTES, function () use ($request) {
           return Link::query()
               ->where('slug', '=', $request->route('link'))
               ->firstOrFail(['slug', 'destination'])
               ->toArray();
        });
        $this->dispatch(new LogAnalytics(request()->getClientIp(), request()->userAgent(), $link['slug']));
        return Redirect::away($link['destination']);
    }
}
