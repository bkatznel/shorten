<?php

namespace App\Http\Requests;

use App\Link;
use Illuminate\Foundation\Http\FormRequest;

class ShowRequest extends FormRequest
{
    public function authorize()
    {
        // You are only able to view links that you have created
        return Link::query()
                ->where('slug', $this->route('link'))
                ->where('user_id', '=', auth()->id())
                ->count() > 0;
    }

    public function rules()
    {
        return [
            //
        ];
    }
}
