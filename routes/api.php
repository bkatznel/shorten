<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use Illuminate\Support\Facades\Route;

Route::middleware(['auth:api'])->group(function () {
    Route::middleware('scopes:link-list')->get('/link', 'LinkController@index');
    Route::middleware('scopes:link-create')->post('/link', 'LinkController@store');
    Route::middleware('scopes:link-show')->get('/link/{link}', 'LinkController@show');
    Route::middleware('scopes:link-analytics')->get('/link/analytics/{link}', 'AnalyticsController');
});
