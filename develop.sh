#!/usr/bin/env bash

# Set environment variables for development
export APP_ENV=${APP_ENV:-local}
export APP_PORT=${APP_PORT:-80}

export DB_PORT=${DB_PORT:-3306}
export MYSQL_ROOT_PASSWORD=${MYSQL_ROOT_PASSWORD:-secret}
export MYSQL_DATABASE=${MYSQL_DATABASE:-homestead}
export MYSQL_USER=${MYSQL_USER:-homestead}
export MYSQL_PASSWORD=${MYSQL_PASSWORD:-secret}

# Decide which compose command & docker-compose file to use
COMPOSE="docker-compose -f ./docker/docker-compose.yml"

# If we pass any arguments...
if [ $# -gt 0 ];then
    #if "t" is used, exec phpunit in the current running api container
    if [ "$1" == "t" ]; then
        shift 1
        $COMPOSE exec \
            api \
            sh -c "cd /var/www/html && ./vendor/bin/phpunit $@"

    # If "bash" is used, start a new bash shell inside the current running api container
    elif [ "$1" == "bash" ]; then
        shift 1
        $COMPOSE exec \
            api \
            bash

    # If "composer" is used, pass-through any additional params to composer within the current running api container
    elif [ "$1" == "composer" ]; then
        shift 1
        $COMPOSE run --rm \
            -w /var/www/html \
            --entrypoint "composer" \
            api \
            $@

    # If "artisan" is used, pass-thru to artisan within the current running api container
    elif [ "$1" == "art" ]; then
        shift 1
        $COMPOSE run --rm \
            -w /var/www/html \
            --entrypoint "php" \
            api \
            artisan $@

    # If "mfs" is used, migrate then refresh and seed the database
    elif [ "$1" == "mfs" ]; then
        shift 1
        $COMPOSE run --rm \
            -w /var/www/html \
            --entrypoint "php artisan migrate:fresh --seed" \
            api \
            $@

    # If "mfs" is used, migrate then refresh and seed the database
    elif [ "$1" == "work" ]; then
        shift 1
        $COMPOSE run --rm \
            -w /var/www/html \
            --entrypoint "php artisan queue:work --queue=default --tries=3" \
            api \
            $@

    # If "xdebug" is used, enable xdebug within the current running api container
    # then restart php-fpm
    elif [ "$1" == "xdebug" ]; then
        shift 1
        $COMPOSE exec \
            api \
            sh -c "phpenmod xdebug \
             && supervisorctl restart php-fpm"

    # If "listener" is used, start a new redis container (redis_1)
    # Then start a new api container as a listener daemon
    # To enable the queue Make sure the following .env vars are set:
    #     QUEUE_DRIVER=redis
    #     REDIS_HOST=redis_1
    #
    # NOTE!!!: "./devlop.sh down" will not stop the redis container. It needs to be shutdown manually.
    elif [ "$1" == "listener" ]; then
        shift 1
        docker run --rm -d --name redis_1 --network deploy_shorten -p 6379:6379 redis
        $COMPOSE run \
            -w /var/www/html \
            --name listener \
            -d \
            -p 8080:80 \
            -e APP_SERVICE=listener \
            --entrypoint "/usr/bin/supervisord -c /etc/supervisor/conf.d/listener.conf" \
            api

    # Else, pass-thru args to docker-compose
    else
        $COMPOSE "$@"
    fi

else
  #Finally, if no arguments are provided run ps
    $COMPOSE ps
fi

