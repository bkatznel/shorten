# README #

URL Shortner

### Setup ###

1. `cp .env.example .env` Setup the environment variables
1. `./develop.sh up -d`  This will start the api & mysql containers as daemons
1. `./develop.sh composer install`  This will install all the composer dependencies
1. `./develop.sh mfs` This will migrate, refresh and seed the database as well as generate oauth2 public and private keys if they do not exist
1. `npm i` Install node dependencies
1. `npm run dev` Transpile javascript assets with webpack

### How do I get started? ###

1. Navigate to `/welcome` to register
1. Generate a personal OAuth2 access token

### Web Routes ###
* GET `/welcome`
  * The home screen. From here you can navigate to the registration and login pages.
* GET `/{link}`
  * If the shortened slug is found you will be redirected to the destination URL.

### API Routes ###
* GET `/api/link`
  * **Scope:** `link-list`
* POST `/api/link` 
  * **Scope:** `link-create`
* GET `/api/link/{link}` 
  * **Scope:** `link-show`
* GET `/api/link/analytics/{link}` 
  * **Scope:** `link-analytics`


### Dependencies ###
* docker + docker-compose
  * installed and the ability for docker to mount the working directory of the project
* node >= 8.70

### Notes ###
These are a few discussion points about the project.

1. After implementing the slug class I noticed that this can also be represented in base62. Using a base62
implementation I could have continued using an autoincrement as my primary key in mysql on the Link Table and
just converted between base62 and base10 when looking up the slug in mysql. That strategy would also have the benefit of
never requiring a re-roll in the case of collisions.

1. If I were to implement this for a real project I would also split up the domains (or sub-domains) for managing the
links. With this implementation (regardless of the slug generation strategy) `/welcome` would cause a conflict. If
administrative urls were moved to another domain then that would remove this issue.

1. The queued LogAnalytics job uses the free Ipstack api which retrieves additional data via the user's ip address.
However, since this is running locally within a docker container the ip address provided is to an internal network
which does not provide the most information. If this were hosted on a server and accessed over the internet the return
from Ipstack would not be null.
